import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import {NoteService} from '../../providers/note-service/note-service';
import { Note } from '../../models/note.model';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ChangeDetectorRef} from '@angular/core';
import {SpeechRecognition} from '@ionic-native/speech-recognition'
/**
 * Generated class for the AddNotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-note',
  templateUrl: 'add-note.html',
})
export class AddNotePage {
  private : boolean;
  saidText : string ='';
  matches: String[];
  isRecording = false;
  formGroup : FormGroup;
  note: Note;
  date: string = '';
  title: string = '';
  content: string = '';


  constructor(public navCtrl: NavController, private noteService: NoteService, private plt: Platform, private speechRecognition: SpeechRecognition, private cd: ChangeDetectorRef) {

    this.formGroup = new FormGroup({
      title: new FormControl(),
      content: new FormControl(),
      date: new FormControl(),
    })
  }
  saveNote(note: Note){
    this.noteService.saveNote(note);
    //pop current view off stack, return to previous view
    this.navCtrl.pop();
  }

  startListening(){
    let options ={
      language : 'nl-NL'
    }
    this.speechRecognition.startListening(options).subscribe(matches =>{
      this.matches = matches;
      this.saidText = matches[0];
      this.cd.detectChanges();
      this.isRecording = false;
    });
    this.isRecording = true;
  }
  stopListening(){
    this.speechRecognition.stopListening().then(()=>{
      this.isRecording = false;
    });
  }

ngOnInit(){
  this.date =  new Date().toLocaleString()
  this.speechRecognition.hasPermission()
  .then((hasPermission:boolean)=>{
    if(!hasPermission){
      this.speechRecognition.requestPermission()
      .then(
        () => console.log("granted"),
        () => console.log("denied")
      )
    }
  });
}


  isIos(){
    return this.plt.is('ios');
  }
}
