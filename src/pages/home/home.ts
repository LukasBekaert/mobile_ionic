import { Component } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';
import { AddNotePage } from '../add-note/add-note';
import { NoteService } from '../../providers/note-service/note-service';
import { Note } from '../../models/note.model';
import { ViewNotePage } from '../view-note/view-note';
import {FingerprintAIO,FingerprintOptions} from '@ionic-native/fingerprint-aio';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  fingerprintOptions : FingerprintOptions
  notes: Promise<Note[]>;

  private note: Note;
  constructor(private platform : Platform, public navCtrl: NavController, private noteService: NoteService , private fingerprint : FingerprintAIO) {
    this.fingerprintOptions = {
      clientId: 'user',
      clientSecret: 'password',
      disableBackup: true
    }

  }
  ionViewWillEnter(){
    this.notes = this.getAllNotes();
  }

 getNote(createDate: number){

  if(this.fingerprint.isAvailable){
      this.fingerprint.show({
        clientId: 'UserFinger',
        clientSecret: "password"
    })
    .then(result => {
      this.noteService.getNote(createDate).then(async (n) => {
        this.note = n;
            this.navCtrl.push(ViewNotePage,{note: this.note})
      })
    })
    .catch(err => {
      console.log(err);
    })
  }
  else{ //indien er geen sensor is werkt deze beveiliging dus niet.
    this.noteService.getNote(createDate).then(async (n) => {
      this.note = n;
          this.navCtrl.push(ViewNotePage,{note: this.note})
    })
  }




  }

  addNote(){
    this.navCtrl.push(AddNotePage);
  }
  getAllNotes(){
    return this.noteService.getAllNotes();
  }

}
